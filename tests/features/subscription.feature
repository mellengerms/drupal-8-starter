Feature: Subscriptions Admin
  In order to test the subscription functionality
  As an administrator
  I need to be able to add subscribers

  @api @javascript
  Scenario: Check that declined credit card fails
    Given I am logged in as a user with the "administrator" role
    When I go to "admin/people/subscriber-admin/new"
    And I press the "Add Payment Option" button
    And I enter a cc with "4000000000000002"
    And I wait "5" seconds
    Then I should see "This card was declined"
#    Then I should see the link "test mode"

#    And I fill in dummy data with email "ben@mellenger.com"
#    Then I should see the error message "Email"

#    And I go to "/admin/people"
#    Then I should see the link "ben+123456@mellenger.com"
#    And I press the "Add Subscriber" button
#    Then I wait "20" seconds
